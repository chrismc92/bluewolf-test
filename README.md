# Bluewolf Coding Challenge #

### Summary ###
This repository houses the webapp I was tasked with coding as part of my round two interview with bluewolf. It's a weather and geolocation mashup for a fictional client called skycast. More details can be seen in the assignment pdf.

### Who do I talk to? ###

* Christian McNamara (mcnamara.christian@gmail.com)